FROM wordpress:5.1.1
COPY . /var/www/html/
COPY wp-content/themes/ /var/www/html/wp-content/themes
COPY wp-config.php /var/www/html/wp-config.php

RUN find /var/www/html -type d -exec chmod 0755 {} \;
RUN find /var/www/html -type f -exec chmod 0644 {} \;
RUN chmod 0777 -R /var/www/html/wp-content/uploads/
