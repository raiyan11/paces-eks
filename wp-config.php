<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
//include ".env.php";


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

$_SERVER['HTTPS'] = 'on';
$_SERVER['SERVER_PORT'] = 443;

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gO?n`aorA0X#8e?i>2w_}J6q]h7lmA|{sffgIa4{{8|SQN)~BC%5~L<Ps5Rn~Zm{' );
define( 'SECURE_AUTH_KEY',  '2q03kZ!t$(M?zTDt+qm4dso^Q%7#K2~C?v>w(.+@e$Ezt$`~,/k3mGficrv*HCI0' );
define( 'LOGGED_IN_KEY',    '&2TIPA^yi__K!2(Q;w JaH]n,0:}@G#E?g[2ViA{F7FOTz,AT_Y Momqdrr@bF-q' );
define( 'NONCE_KEY',        '[zNE>**xW^S%~2R5C`G+>qY~MIx37/xQ;)jfp:?Aek*L}R/Vb:95E:5Mzz=bFPmq' );
define( 'AUTH_SALT',        'o(Qe L9Y4EZ~)/3OTamMOk4chw&z|L#5J1pl-MhQ6<vgr<dk]3E3Dv{VG3J11`F#' );
define( 'SECURE_AUTH_SALT', 'lIjRE>4;Sq`*3tr+PBdh^Cb9YH{rO|c{`L/8$kdQs_-tU5F-/y0I|Ai*@t$eK1sJ' );
define( 'LOGGED_IN_SALT',   '[8n-OEJtNz90sWbS9UThE;}@#SS-[BIES,IdPH~51:>xKb)y0*}_k`oBjp<sxi=S' );
define( 'NONCE_SALT',       'i}U|L B3A.@Yb|FrLMWG{<!Uf0#l.MKA<rpX|h|7q.d;8sA1z_D7}L2XxQ@ny13!' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
